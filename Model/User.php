<?php

namespace SpartakusMd\SocialUserBundle\Model;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Storage agnostic user object.
 */
abstract class User extends BaseUser implements UserInterface
{
    /**
     * The purpose of this unique placeholder element is its use in a column
     * like `emailCanonical` which is mapped as `unique` and thus cannot be
     * set to NULL. Since this application implements both a traditional and
     * a social login, the user must have the choice to not provide an email
     * address.
     *
     * By means of overriding the setters for both the `emailCanonical` and
     * `email` properties, this is handled transparently.
     */
    const PLACEHOLDER_SUFFIX = '@not.set';

    protected function createUniquePlaceholder()
    {
        return md5(time() . rand(0, 99999)) . self::PLACEHOLDER_SUFFIX;
    }

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $usernameSlug;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $identities;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $emailChangeRequests;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->identities = new ArrayCollection();
        $this->emailChangeRequests = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        if (!$email) {
            $this->setEmailCanonical(null);
        }
        if (null === $email) {
            $email = '';
        }

        return parent::setEmail($email);
    }

    /**
     * {@inheritdoc}
     */
    public function setEmailCanonical($emailCanonical)
    {
        $emailCanonical = $emailCanonical ?: $this->createUniquePlaceholder();

        return parent::setEmailCanonical($emailCanonical);
    }

    /**
     * {@inheritdoc}
     */
    public function setUsernameSlug($usernameSlug)
    {
        $this->usernameSlug = $usernameSlug;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsernameSlug()
    {
        return $this->usernameSlug;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function addIdentity(UserIdentity $identity)
    {
        $this->identities[] = $identity;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeIdentity(UserIdentity $identity)
    {
        $this->identities->removeElement($identity);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentities()
    {
        return $this->identities;
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentitiesAsStrings()
    {
        $result = array();
        foreach ($this->identities as $identity) {
            $result[] = $identity->getTypeString();
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function addEmailChangeRequest(UserEmailChangeRequest $emailChangeRequest)
    {
        $this->emailChangeRequests[] = $emailChangeRequest;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeEmailChangeRequest(UserEmailChangeRequest $emailChangeRequest)
    {
        $this->emailChangeRequests->removeElement($emailChangeRequest);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmailChangeRequests()
    {
        return $this->emailChangeRequests;
    }
}
