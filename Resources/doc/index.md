* [Installation and configuration](https://bitbucket.org/SpartakusMd/socialuserbundle/src/master/Resources/doc/installation_and_configuration.md)
* [Connecting user accounts](https://bitbucket.org/SpartakusMd/socialuserbundle/src/master/Resources/doc/connect.md)

For anything not covered in this documentation, please refer to the documentations of
the [FOSUserBundle](https://github.com/FriendsOfSymfony/FOSUserBundle),
the [HWIOAuthBundle](https://github.com/hwi/HWIOAuthBundle),
and [Symfony](http://www.symfony.com) itself.
