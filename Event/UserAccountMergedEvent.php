<?php

namespace SpartakusMd\SocialUserBundle\Event;

use SpartakusMd\SocialUserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\Event;

class UserAccountMergedEvent extends Event
{
    const ID = 'security.user_accounts_merged';

    /**
     * @var \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    protected $mergingUser;

    /**
     * @var \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    protected $mergedUser;

    /**
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function getMergingUser()
    {
        return $this->mergingUser;
    }

    /**
     * @param \SpartakusMd\SocialUserBundle\Model\UserInterface $user
     */
    public function setMergingUser(UserInterface $user)
    {
        $this->mergingUser = $user;
    }

    /**
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function getMergedUser()
    {
        return $this->mergedUser;
    }

    /**
     * @param \SpartakusMd\SocialUserBundle\Model\UserInterface $user
     */
    public function setMergedUser(UserInterface $user)
    {
        $this->mergedUser = $user;
    }
}
