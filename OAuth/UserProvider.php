<?php

namespace SpartakusMd\SocialUserBundle\OAuth;

use SpartakusMd\SocialUserBundle\Model\User;
use SpartakusMd\SocialUserBundle\Model\UserIdentity;
use SpartakusMd\SocialUserBundle\Model\StorageAgnosticObjectManager;
use SpartakusMd\SocialUserBundle\Event\UserAccountMergedEvent;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserProvider implements OAuthAwareUserProviderInterface
{
    /** @var UserManagerInterface */
    protected $userManager;

    /** @var StorageAgnosticObjectManager */
    protected $identityManager;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /**
     * Constructor.
     *
     * @param UserManagerInterface $userManager FOSUB User manager
     * @param StorageAgnosticObjectManager $identityManager Identity manager
     * @param EventDispatcherInterface $eventDispatcher Event dispatcher
     */
    public function __construct(
        UserManagerInterface $userManager,
        StorageAgnosticObjectManager $identityManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->userManager = $userManager;
        $this->identityManager = $identityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function connect($user, UserResponseInterface $response)
    {
        $existingIdentity = $this->getExistingIdentity($response);
        if ($existingIdentity) {
            $previousUser = $existingIdentity->getUser();
            $event = new UserAccountMergedEvent('User accounts merged');
            $event->setMergedUser($previousUser);
            $event->setMergingUser($user);

            $this->eventDispatcher->dispatch(UserAccountMergedEvent::ID, $event);

            $existingIdentity->setUser($user);
            $existingIdentity->setAccessToken($this->getAccessToken($response));
            $this->identityManager->update($existingIdentity);
        } else {
            $this->createIdentity($user, $response);
        }
    }

    /**
     * @param string $slug
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function findOneByUsernameSlug($slug)
    {
        $criteria = array('usernameSlug' => $slug);

        return $this->userManager->findUserBy($criteria);
    }

    /**
     * @param string $email
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function findOneByEmail($email)
    {
        $criteria = array('emailCanonical' => strtolower($email));

        return $this->userManager->findUserBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $existingIdentity = $this->getExistingIdentity($response);
        if ($existingIdentity) {
            $existingIdentity->setAccessToken($this->getAccessToken($response));

            return $existingIdentity->getUser();
        }

        $email = $response->getEmail();
        if (!empty($email) && $existingUser = $this->findOneByEmail($email)) {
            $this->createIdentity($existingUser, $response);

            return $existingUser;
        }

        return $this->createUser($response);
    }

    /**
     * Checks whether the authenticating Identity already exists.
     *
     * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserIdentity
     */
    protected function getExistingIdentity(UserResponseInterface $response)
    {
        return $this->identityManager->findOneBy(array(
            'identifier' => $response->getUsername(),
            'type' => $response->getResourceOwner()->getName(),
        ));
    }

    /**
     * Creates new User.
     *
     * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
     *
     * @return \SpartakusMd\SocialUserBundle\Model\User
     */
    protected function createUser(UserResponseInterface $response)
    {
        $user = $this->userManager->createUser();
        $user->setUsername($this->createUniqueUsername($response->getRealName()));
        $user->setEmail($response->getEmail());
        $user->setPassword('');
        $user->setEnabled(true);
        $this->userManager->updateUser($user);
        $this->createIdentity($user, $response);

        return $user;
    }

    /**
     * Creates new Identity.
     *
     * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
     * @param \SpartakusMd\SocialUserBundle\Model\User $user
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserIdentity
     */
    protected function createIdentity(User $user, UserResponseInterface $response)
    {
        $identity = $this->identityManager->create();
        $identity->setAccessToken($this->getAccessToken($response));
        $identity->setIdentifier($response->getUsername());
        $identity->setType($response->getResourceOwner()->getName());
        $identity->setUser($user);
        $identity->setName($response->getRealName());
        $identity->setEmail($response->getEmail());
        $this->identityManager->update($identity);

        return $identity;
    }

    /**
     * Ensures uniqueness of username.
     *
     * @param string $username
     *
     * @return string
     */
    protected function createUniqueUsername($username)
    {
        $originalName = $username;
        $existingUser = $this->userManager->findUserByUsername($username);
        $suffix = 0;
        while ($existingUser) {
            ++$suffix;
            $username = $originalName . $suffix;
            $existingUser = $this->userManager->findUserByUsername($username);
        }

        return $username;
    }

    /**
     * Workaround method for HWIOAuthBundle issue.
     *
     * Waiting for this issue to be fixed upstream
     *
     * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
     *
     * @return string
     */
    protected function getAccessToken(UserResponseInterface $response)
    {
        $accessToken = $response->getAccessToken();
        switch ($response->getResourceOwner()->getName()) {
            //case UserIdentity::getReadableType( UserIdentity::TYPE_TWITTER ):
            case UserIdentity::getReadableType(UserIdentity::TYPE_YAHOO):
                return $accessToken['oauth_token'];
            default:
                return $accessToken;
        }
    }
}
